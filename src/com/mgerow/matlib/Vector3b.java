package com.mgerow.matlib;

public final class Vector3b {
    public final boolean x, y, z;

    public Vector3b() {
	this.x = this.y = this.z = false;
    }

    public Vector3b(boolean x, boolean y, boolean z) {
	this.x = x;
	this.y = y;
	this.z = z;
    }

    public Vector3b(boolean all) {
	this.x = this.y = this.z = all;
    }

    public Vector3b or(Vector3b other) {
	return new Vector3b(x || other.x, y || other.y, z || other.z);
    }

    public Vector3b or(boolean scalar) {
	return new Vector3b(x || scalar, y || scalar, z || scalar);
    }

    public Vector3b and(boolean scalar) {
	return new Vector3b(x && scalar, y && scalar, z && scalar);
    }

    public Vector3b and(Vector3b other) {
	return new Vector3b(x && other.x, y && other.y, z && other.z);
    }

    public boolean dot(Vector3b other) {
	return x && other.x || y && other.y || z && other.z;
    }

    public boolean equals(Vector3b other) {
	return (this.x == other.x && this.y == other.y && this.z == other.z);
    }

    public boolean[] toArray() {
	boolean[] out = new boolean[3];
	out[0] = x;
	out[1] = y;
	out[2] = z;

	return out;
    }

    public String toString() {
	return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }
}
