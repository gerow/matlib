package com.mgerow.matlib;

import java.util.ArrayList;

public class Matrix4fStack {
    private ArrayList<Matrix4f> stack = new ArrayList<Matrix4f>();

    public Matrix4fStack() {
	stack.add(Matrix4f.identity());
    }

    public Matrix4fStack(Matrix4f top) {
	stack.add(top);
    }

    public Matrix4f top() {
	return this.getTop();
    }

    private Matrix4f getTop() {
	return stack.get(stack.size() - 1);
    }

    private void setTop(Matrix4f element) {
	this.stack.set(stack.size() - 1, element);
    }

    public void set(Matrix4f element) {
	this.setTop(element);
    }

    public void multiply(Matrix4f other) {
	this.setTop(other.multiply(this.getTop()));
    }

    public void push() {
	this.stack.add(this.getTop());
    }
    
    public void pop() {
	this.stack.remove(this.stack.size() - 1);
    }
}
