package com.mgerow.matlib;

public final class Vector4b {
    public final boolean x, y, z, w;

    public Vector4b() {
	this.x = this.y = this.z = this.w = false;
    }

    public Vector4b(boolean x, boolean y, boolean z, boolean w) {
	this.x = x;
	this.y = y;
	this.z = z;
	this.w = w;
    }

    public Vector4b(boolean all) {
	this.x = this.y = this.z = this.w = all;
    }

    public Vector4b or(Vector4b other) {
	return new Vector4b(x || other.x, y || other.y, z || other.z, w || other.w);
    }

    public Vector4b or(boolean scalar) {
	return new Vector4b(x || scalar, y || scalar, z || scalar, w || scalar);
    }

    public Vector4b and(boolean scalar) {
	return new Vector4b(x && scalar, y && scalar, z && scalar, w && scalar);
    }

    public Vector4b and(Vector4b other) {
	return new Vector4b(x && other.x, y && other.y, z && other.z, w && other.w);
    }

    public boolean dot(Vector4b other) {
	return x && other.x || y && other.y || z && other.z || w && other.w;
    }

    public boolean equals(Vector4b other) {
	return (this.x == other.x && this.y == other.y && this.z == other.z && this.w == other.w);
    }

    public boolean[] toArray() {
	boolean[] out = new boolean[4];
	out[0] = x;
	out[1] = y;
	out[2] = z;
	out[3] = w;

	return out;
    }
    
    public String toString() {
	return "(" + this.x + ", " + this.y + ", " + this.z + ", " + this.w + ")";
    }
}
