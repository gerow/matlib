package com.mgerow.matlib;

public final class Vector4f {
    public final float x, y, z, w;

    public Vector4f() {
	this.x = this.y = this.z = 0.0f;
	this.w = 1.0f;
    }

    public Vector4f(float x, float y, float z, float w) {
	this.x = x;
	this.y = y;
	this.z = z;
	this.w = w;
    }
    
    public Vector4f(float x, float y, float z) {
	this.x = x;
	this.y = y;
	this.z = z;
	this.w = 1.0f;
    }

    public Vector4f(float allButW) {
	this.x = this.y = this.z = allButW;
	this.w = 1.0f;
    }
    
    public Vector4f(float allButW, float w) {
	this.x = this.y = this.z = allButW;
	this.w = w;
    }
    
    public Vector4f(Vector3f other) {
	this.x = other.x;
	this.y = other.y;
	this.z = other.z;
	this.w = 1.0f;
    }
    
    public Vector4f(Vector3f other, float w) {
	this.x = other.x;
	this.y = other.y;
	this.z = other.z;
	this.w = w;
    }

    public Vector4f add(Vector4f other) {
	return new Vector4f(x + other.x, y + other.y, z + other.z, w + other.w);
    }
    
    public Vector4f add(float value) {
	return new Vector4f(x + value, y + value, z + value, w + value);
    }

    public Vector4f multiply(float scalar) {
	return new Vector4f(x * scalar, y * scalar, z * scalar, w * scalar);
    }

    public Vector4f multiply(Vector4f other) {
	return new Vector4f(x * other.x, y * other.y, z * other.z, w * other.w);
    }

    public Vector4f subtract(Vector4f other) {
	return this.add(other.multiply(-1));
    }
    
    public Vector4f subtract(float value) {
	return new Vector4f(x - value, y - value, z - value, w - value);
    }

    public Vector4f divide(float scalar) {
	return this.multiply(1.0f / scalar);
    }
    
    public Vector4f divide(Vector4f other) {
	return new Vector4f(x / other.x, y / other.y, z / other.z, w / other.w);
    }

    public float dot(Vector4f other) {
	return x * other.x + y * other.y + z * other.z + w * other.w;
    }

    public Vector4f normalize() {
	float dist = (float) Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)
		+ Math.pow(z, 2) + Math.pow(w, 2));
	return new Vector4f(x / dist, y / dist, z / dist, w / dist);
    }

    public Vector4f vectorTo(Vector4f other) {
	return other.subtract(this);
    }

    public float distanceTo(Vector4f other) {
	return (float) Math.sqrt(Math.pow(other.x - x, 2)
		+ Math.pow(other.y - y, 2) + Math.pow(other.z - z, 2) + Math.pow(other.w - w, 2));
    }

    public boolean equals(Vector4f other) {
	return (this.x == other.x && this.y == other.y && this.z == other.z && this.w == other.w);
    }

    public float magnitude() {
	return (float) Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2)
		+ Math.pow(this.z, 2) + Math.pow(this.w, 2));
    }
    
    public Vector3f dehomogenize() {
	return new Vector3f(x / w, y / w, z / w);
    }


    public float[] toArray() {
	float[] out = new float[4];
	out[0] = x;
	out[1] = y;
	out[2] = z;
	out[3] = w;

	return out;
    }

    public String toString() {
	return "(" + this.x + ", " + this.y + ", " + this.z + ", " + this.w + ")";
    }
}

