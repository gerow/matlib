package com.mgerow.matlib;

import java.util.List;

public final class Matrix4f {
    private float[] data = new float[16];

    public Matrix4f() {
	for (int i = 0; i < data.length; ++i)
	    data[i] = 0.0f;
    }

    public Matrix4f(float[] data) {
	if (data.length != this.data.length)
	    throw new RuntimeException(
		    "Invalid initial data for Matrix4f constructor (" + data.length + ")");
	for (int i = 0; i < this.data.length; ++i)
	    this.data[i] = data[i];
    }

    public Matrix4f(List<Float> data) {
	if (data.size() != this.data.length)
	    throw new RuntimeException(
		    "Invalid initial data for Mat3f constructor");
	for (int i = 0; i < this.data.length; ++i)
	    this.data[i] = data.get(i);
    }

    public Matrix4f(Vector4f a, Vector4f b, Vector4f c, Vector4f d) {
	this.data[0] = a.x;
	this.data[1] = a.y;
	this.data[2] = a.z;
	this.data[3] = a.w;

	this.data[4] = b.x;
	this.data[5] = b.y;
	this.data[6] = b.z;
	this.data[7] = b.w;

	this.data[8] = c.x;
	this.data[9] = c.y;
	this.data[10] = c.z;
	this.data[11] = c.w;
	
	this.data[12] = d.x;
	this.data[13] = d.y;
	this.data[14] = d.z;
	this.data[15] = d.w;
    }

    public Vector4f multiply(Vector4f other) {
	float x, y, z, w;

	x = data[0] * other.x + data[4] * other.y + data[8] * other.z + data[12] * other.w;
	y = data[1] * other.x + data[5] * other.y + data[9] * other.z + data[13] * other.w;
	z = data[2] * other.z + data[6] * other.y + data[10] * other.z + data[14] * other.w;
	w = data[3] * other.z + data[7] * other.y + data[11] * other.z + data[15] * other.w;

	return new Vector4f(x, y, z, w);
    }
    
    public Vector3f multiply(Vector3f other) {
	Vector4f temp = other.homogenize();
	temp = this.multiply(temp);
	
	return temp.dehomogenize();
    }

    private float accessRowColum(int i, int j) {
	return data[i + 4 * j];
    }

    private void putRowColumn(int i, int j, float value) {
	data[i + 4 * j] = value;
    }

    public Matrix4f multiply(Matrix4f other) {
	Matrix4f out = new Matrix4f();
	for (int i = 0; i < 4; ++i) {
	    for (int j = 0; j < 4; ++j) {
		float value = 0;
		for (int k = 0; k < 4; ++k) {
		    value += this.accessRowColum(i, k)
			    * other.accessRowColum(k, j);
		}
		out.putRowColumn(i, j, value);
	    }
	}
	
	return out;
    }

    public boolean equals(Matrix4f other) {
	for (int i = 0; i < this.data.length; ++i) {
	    if (this.data[i] != other.data[i])
		return false;
	}
	return true;
    }

    /**
     * Returns an array representing the matrix in column major form.
     * @return
     */
    public float[] toArray() {
	float[] out = new float[this.data.length];
	for (int i = 0; i < this.data.length; ++i)
	    out[i] = data[i];
	return out;
    }

    public float[] toArrayRowMajor() {
	float[] out = new float[this.data.length];
	for (int i = 0; i < 4; ++i) {
	    out[4 * i] = this.accessRowColum(i, 0);
	    out[4 * i + 1] = this.accessRowColum(i, 1);
	    out[4 * i + 2] = this.accessRowColum(i, 2);
	    out[4 * i + 3] = this.accessRowColum(i, 3);
	}

	return out;
    }

    public Vector4f[] toVectorArray() {
	Vector4f[] out = new Vector4f[4];
	
	out[0] = new Vector4f(this.data[0], this.data[1], this.data[2], this.data[3]);
	out[1] = new Vector4f(this.data[4], this.data[5], this.data[6], this.data[7]);
	out[2] = new Vector4f(this.data[8], this.data[9], this.data[10], this.data[11]);
	out[3] = new Vector4f(this.data[12], this.data[13], this.data[14], this.data[15]);

	return out;
    }

    public String toString() {
	String out;
	out = "[ " + data[0] + ", " + data[4] + ", " + data[8] + ", " + data[12] + " ]\n";
	out += "[ " + data[1] + ", " + data[5] + ", " + data[9] + ", " + data[13] + " ]\n";
	out += "[ " + data[2] + ", " + data[6] + ", " + data[10] + ", " + data[14] + " ]\n";
	out += "[ " + data[3] + ", " + data[7] + ", " + data[11] + ", " + data[15] +  " ]";

	return out;
    }

    public static Matrix4f zero() {
	return new Matrix4f();
    }

    public static Matrix4f identity() {
	Matrix4f out = new Matrix4f();
	for (int i = 0; i < out.data.length; ++i) {
	    if (i == 0 || i == 5 || i == 10 || i == 15)
		out.data[i] = 1.0f;
	    else
		out.data[i] = 0.0f;
	}
	return out;
    }

    public static void main(String args[]) {
	float[] arr = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f, 16.0f };
	Matrix4f mat1 = Matrix4f.identity();
	Matrix4f mat2 = new Matrix4f(arr);

	System.out.println(mat1 + "\n");
	System.out.println(mat2);

	System.out.println(mat1.accessRowColum(0, 0));
	System.out.println(mat1.multiply(mat2));
    }
}
