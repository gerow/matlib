package com.mgerow.matlib;

public final class Vector2b {
    public final boolean x, y;

    public Vector2b() {
	this.x = this.y = false;
    }

    public Vector2b(boolean x, boolean y) {
	this.x = x;
	this.y = y;
    }

    public Vector2b(boolean all) {
	this.x = this.y = all;
    }

    public Vector2b or(Vector2b other) {
	return new Vector2b(x || other.x, y || other.y);
    }

    public Vector2b or(boolean scalar) {
	return new Vector2b(x || scalar, y || scalar);
    }

    public Vector2b and(boolean scalar) {
	return new Vector2b(x && scalar, y && scalar);
    }

    public Vector2b and(Vector2b other) {
	return new Vector2b(x && other.x, y && other.y);
    }

    public boolean dot(Vector2b other) {
	return x && other.x || y && other.y;
    }

    public boolean equals(Vector2b other) {
	return (this.x == other.x && this.y == other.y );
    }

    public boolean[] toArray() {
	boolean[] out = new boolean[2];
	out[0] = x;
	out[1] = y;

	return out;
    }

    public String toString() {
	return "(" + this.x + ", " + this.y + ")";
    }
}